const router = require('express').Router();
const feedRoute = require('./../components/feeds/feed.route');
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const authenticate = require('./../middlewares/authenticate');
const authorize = require("./../middlewares/authorize");

router.use('/auth', authRoute);
router.use('/feed', feedRoute);
router.use('/user', authenticate, authorize, userRoute);

module.exports = router;


