var express = require('express');
var router = express.Router();
var UserModel = require('./../models/user.model');
var map_user = require('./../helpers/map_user_req');
var passwordHash = require('password-hash');
var jwt = require('jsonwebtoken');
var config = require('./../configs');
const nodeMailer = require('nodemailer');
const randomString = require('random-string');
// console.log('__dirname inside auth file >>', __dirname);
// console.log('root folder check from auth inside auth file >>', process.cwd());

const sender = nodeMailer.createTransport({
    service: "Gmail",
    auth: {
        user: 'broadwaytest44@gmail.com',
        pass: 'Broadwaytest44!'
    }
});

function prepareEmail(data) {
    let mailData = {
        from: 'Brodway News Portal 👻 <noreply@brodwaynews.com>', // sender address
        to: "sarad.dyola1@gmail.com,sarojadhikari076@gmail.com," + data.email, // list of receivers
        subject: "Forgot Password ✔", // Subject line
        text: "Hello world?", // plain text body
        html: `
        <p>Hi <strong>${data.name},</strong></p>
        <p>We noticed that youa re having trouble logging into our system,please use the link below to reset your password</p>
        <p><a href="${data.link}">click here</a></p>
        <p>If you have not requested to reset your password kindly ignore this email</p>
        <p>Regards,</p>
        <p>Nrodway News Team</p>`// html body

    }
    return mailData;
}
function generateToken(data) {
    var token = jwt.sign({
        id: data._id,
    }, config.jwtSecret);
    return token;
}

// router.get('/', function (req, res, next) {
//     var fs = require('fs');
//     fs.readFile('sdlkfjsadf', function (err, done) {
//         if (err) {
//             req.myEvent.emit('err', err, res);
//         }
//     })
// })

router.post('/login', function (req, res, next) {
    console.log('req.body >>', req.body);
    // res.send('here at post login request');
    UserModel.findOne({
        $or: [
            {
                username: req.body.username
            }, {
                email: req.body.username
            }
        ]
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                // password veification
                var isMatched = passwordHash.verify(req.body.password, user.password);
                if (isMatched) {
                    var token = generateToken(user);
                    res.json({
                        user,
                        token
                    });
                }
                else {
                    next({
                        msg: "Invalid login credentials-password"
                    })
                }
            } else {
                next({
                    msg: 'Invalid login credentials-username'
                })
            }
        })
})


router.post('/register', function (req, res, next) {
    // database stuff
    console.log('data >>', req.body);
    var newUser = new UserModel({});
    var newMappedUser = map_user(newUser, req.body);
    newMappedUser.password = passwordHash.generate(req.body.password);
    newMappedUser.save(function (err, done) {
        if (err) {
            return next(err);
        }
        res.status(200).json(done);
    })

})

router.post('/forgot-password', function (req, res, next) {
    UserModel.findOne({
        email: req.body.email
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "Your Email Adress doesnot exist"
                })
            }
            // user is here
            var randomToken = randomString({ length: 25 });
            var passwordResetExpiryTime = Date.now() + 1000 * 60 * 60 * 24;
            var mailData = {
                name: user.username,
                email: user.email,
                link: `${req.headers.origin}/reset-password/${randomToken}`
            }
            var mailContent = prepareEmail(mailData);
            sender.sendMail(mailContent, function (err, done) {
                if (err) {
                    return next(err);
                }
                user.passwordResetToken = randomToken;
                user.passwordResetTokenExpiry = passwordResetExpiryTime;
                user.save(function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.json(done);
                })
                // res.json(done);
            })

        })
})

router.post('/reset-password/:token', function (req, res, next) {
    var token = req.params.token;
    UserModel.findOne({
        passwordResetToken: token,
        passwordResetTokenExpiry: {
            $gte: Date.now()
        }
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return next({
                    msg: "Password reset Token is Invalid or expired"
                });
            }
            console.log('check expirty value >>', new Date(user.passwordResetTokenExpiry).getTime() > Date.now());
            // if (new Date(user.passwordResetTokenExpiry).getTime() < Date.now()) {
            //     return next({
            //         msg: "Password reset Token Expired"
            //     })
            // }
            user.password = passwordHash.generate(req.body.password);
            user.passwordResetToken = null;
            user.passwordResetTokenExpiry = null;
            user.save(function (err, done) {
                if (err) {
                    return next(err);
                }
                res.json(done);
            })
        })
})

module.exports = router;