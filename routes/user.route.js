var router = require('express').Router();
var UserModel = require('./../models/user.model');
var map_user = require('./../helpers/map_user_req');
const authorize = require("./../middlewares/authorize");

router.route('/')
    .get(function (req, res, next) {
        console.log('req.loggediNuser >>', req.loggedInUser);
        UserModel
            .find({})
            // .limit(2)
            .sort({ _id: -1 })
            // .skip(1)
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }
                res.status(200).json(users);
            })
    })
    .post(function (req, res, next) {
        // 
    });
router.route('/profile')
    .get(function (req, res, next) {
        res.send('hi from user profile');

    })
    .post(function (req, res, next) {

    })

router.route('/change-password')
    .get(function (req, res, next) {
        res.send('hi from user change-password');

    })
    .post(function (req, res, next) {

    })

router.route('/:id')
    .get(function (req, res, next) {
        UserModel.findOne({ _id: req.params.id })
            .exec(function (err, user) {
                if (err) {
                    return next(err);
                }
                res.status(200).json(user);
            })
    })
    .put(function (req, res, next) {
        var id = req.params.id;
        UserModel.findById(id)
            .exec(function (err, user) {
                if (err) {
                    return next(err);
                }
                if (user) {
                    // console.log('user >>', user)
                    var updatedMappedUser = map_user(user, req.body);
                    updatedMappedUser.save(function (err, updated) {
                        if (err) {
                            return next(err);
                        }
                        res.status(200).json(updated);
                    })
                }
                else {
                    next({
                        msg: 'User not found'
                    })
                }
            })
    })
    .delete(authorize, function (req, res, next) {

        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                user.remove(function (err, removed) {
                    if (err) {
                        return next(err);
                    }
                    res.status(200).json(removed);
                })
            }
            else {
                next({
                    msg: 'User not found'
                })
            }
        })
    });

module.exports = router;

