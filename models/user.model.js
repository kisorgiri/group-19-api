var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    // db modeling part
    name: String,
    email: {
        type: String,
        unique: true
    },
    username: {
        type: String,
        required: true,
        unique: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true,
    },
    phone: Number,
    dob: Date,
    address: String,
    gender: {
        type: String,
        enum: ['male', 'female', 'others'],
    },
    role: {
        type: Number, //1 admin, 2 normal ,3 visitor
        default: 2
    },
    status: {
        type: String,
        default: 'active'
    },
    passwordResetToken: String,
    passwordResetTokenExpiry: Date
}, {
    timestamps: true
})

var UserModel = mongoose.model('user', userSchema);

module.exports = UserModel;