var events = require('events');
// import events from 'events';

var myEvent = new events.EventEmitter();
var myEvent1 = new events.EventEmitter();
var myEvent2 = new events.EventEmitter();

myEvent1.on('test', function () {
    console.log('i am on test event')
})

setTimeout(function () {
    myEvent.emit('test');

}, 2000);