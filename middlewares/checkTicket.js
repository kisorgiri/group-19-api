module.exports = function (req, res, next) {
    if (req.query.ticket) {
        return next();
    } else {
        next({
            msg: 'you dont have ticket',
            status: 400
        })
    }
}