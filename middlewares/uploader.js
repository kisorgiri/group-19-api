const multer = require('multer');
var path = require('path');

// const upload= multer({
//     dest:'./uploads/'
// });
// diskstorage for full control of filename and location
const storeConfig = multer.diskStorage({
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname);
    },
    destination: function (req, file, cb) {
        cb(null, path.join(process.cwd(), './uploads/images'));
    }
});
function filter(req, file, cb) {
    var mimeType = file.mimetype.split('/')[0];
    if (mimeType != 'image') {
        req.fileError = true;
        cb(null, false);
    } else {
        cb(null, true);
    }
}
var upload = multer({
    storage: storeConfig,
    fileFilter: filter
});
module.exports = upload;