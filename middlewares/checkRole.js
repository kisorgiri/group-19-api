module.exports = function (req, res, next) {

    if (req.query.role == 1) {
        return next();
    }
    else {
        next({
            msg: 'you dont have permission',
            status: 403
        })
    }
}