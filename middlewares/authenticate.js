var jwt = require('jsonwebtoken');
var config = require('./../configs');
var UserModel = require('./../models/user.model');

module.exports = function(req, res, next) {
    var token;
    if (req.headers['token']) {
        token = req.headers.token;
    }
    if (req.headers['authorization']) {
        token = req.headers['authorization'];
    }
    if (req.headers['x-access-token']) {
        token = req.headers['x-access-token']
    }
    if (req.query.token) {
        token = req.query.token;
    }
    if (token) {
        jwt.verify(token, config.jwtSecret, function(err, decoded) {
            if (err) {
                return next(err);
            }
            console.log('decoded >>', decoded);
            // req.loggedInUser = decoded;
            // return next();
            UserModel.findById(decoded.id)
                .exec(function(err, user) {
                    if (err) {
                        return next(err);
                    }
                    if (user) {
                        req.loggedInUser = user;
                        return next();
                    } else {
                        next({
                            msg: "user removed from system"
                        })
                    }
                })
        })
    } else {
        next({
            msg: 'Token not provided'
        })
    }
}