const express = require('express');
const app = express(); // your entire express framework is in app
const morgan = require('morgan');
const path = require('path');
const cors = require('cors');

// load routing level middleware
const apiRoute = require('./routes/api.route');

// const event = require('events');
// const myEvent = new event.EventEmitter();

// app.use(function (req, res, next) {
//     req.myEvent = myEvent;
//     next();
// })

// myEvent.on('err', function (data,res) {
//     // console.log('error >>', data);
//     res.json(data);
// })
var users = [];

// socket stuff
const socket = require('socket.io');
const io = socket(app.listen(8081));
io.on('connection', function (client) {
    var id = client.id;
    console.log('socket client connected to server');
    client.on('new-msg', function (data) {
        console.log('data from FE >', data);
        client.emit('reply-msg-own', data); // own client
        client.broadcast.to(data.receiverId).emit('reply-msg-user', data); // aafu bahek sabai client lai
    })
    client.on('new-user', function (user_name) {
        users.push({
            id: id,
            name: user_name
        });
        client.emit('users', users);
        client.broadcast.emit('users', users);
    })
    client.on('is-typing', function (data) {
        console.log('data in typing >>',data);
        client.broadcast.to(data.receiverId).emit('typing', data);
    })
    client.on('is-typing-stop', function (data) {
        console.log(data);
        client.broadcast.to(data.receiverId).emit('typing-stop', data);
    })

    client.on('disconnect', function () {
        users.forEach(function (user, i) {
            if (user.id === id) {
                users.splice(i, 1);
            }
        })
        client.emit('users', users);
        client.broadcast.emit('users', users);
    })
})

// load db
require('./db');

app.use(morgan('dev'));
app.use(cors());
//all the end point exposed from server

// inbuilt middleware
app.use(express.urlencoded({
    extended: true,
}));
app.use(express.json());
// parse incoming form data and add those data in req.body property
app.use(express.static('files/images'))
// it will server content of images which can be used internally within express application
// app.use('/file', express.static('files/images'));
app.use('/file', express.static(path.join(__dirname, 'uploads/images')))
// console.log('__dirname it will give directory folder name >>',__dirname);

app.use('/api', apiRoute);


// 404 error handler
app.use(function (req, res, next) {
    // res.json({
    //     status: 404,
    //     msg: 'Not Found'
    // })
    next({
        status: 404,
        msg: 'Not Found'
    })
});
// error handling middleware
app.use(function (err, req, res, next) {
    console.log('i am error handling middleware', err);
    // this middleware came into action whenever next with argument is called
    // every error response must be sent through error handling middleware
    res.status(err.status || 400);
    res.json({
        msg: err.msg || err,
        status: err.status || 400,
    });
})


const port = process.env.PORT || 8080;

app.listen(port, function (err, done) {
    if (err) {
        console.log('server listening failed');
    } else {
        console.log('server listening at port ' + port);
        console.log('press CTRL +C to exit ');
    }
});


// middleware
// middleware is a function that has acces to http request object http response object
// and refrence of  next middleware function
// syntax
// function (req,res,next){
// req or 1st argument is http request object
// res or 2nd argument is http response Object
// next or 3rd argument is next middleware function refrence
// }
// coniguration
// app.use block is used to configure middleware

// the order of middleware matters

// types of middleware
// 1. application level middleware
// any middelware which can access  and play with requrest and reponse object in application
// 2.routing level middleware
// 3. error handling middleware
// 4. inbuilt middleware
// 5.third party middleware