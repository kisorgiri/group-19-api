// ensure that mongodb is running in your system
// command to access mongo shell
// mongo
// if > angle bracket arrow head notation appears you are ready to go with shell command

// shell command
// show dbs ==> list all the available database of system
// use <db_name> if(db_name exuist) select existing db else create new db
// db shows selected db
// show collections // list all the availble collections
// add new collections
// db.<col_name>.insert({valid json object});
// insert method can hold array as well
// insertMany // 

// fetch from database
// db.<col_name>.find({query_builder})
// .count() // count the document
// .pretty() // format the output

// update and delete
// update
// db.<col_name>.update({brand:'honda'},{$set:{status:'sold'}},{multi:true,upsert:true})
// first object is query builder
// 2nd object must contain $set as key and object as value
// and that object must have key-value pair to be updated
// 3rd object is optional and used to provide optional value
// eg.multi:true

// remove
// db.<col_name>.remove({condition}) // dont leave it empty

// to drop collection
// db.<col_name>.drop();
// to drop entire database 
// db.dropDatabase();

//############# database BACKUP & RESTORE #############
// bson data human readable data==> json ,csv
// BACKUP (bson data)
// mongodump mongodump will backup all the database into default dump folder
// mongodump --db <db_name> it will backup selected database into default dump folder
// mongodump --db <db_name> --out <path_to_destination_folder>.

// restore(bson)
// mongorestore  it will restore all the database available under dump folder
// mongorestore <path_to_containing_folder> // restore from selected folder

// mongodump and mongorestore always cames in pair

// csv &json///
// mongoexport and mongoimport always cames in pair

// json
// backup
// mongoexport --db <db_name> --collection <col_name> --out <path_to_destination_with_.json extension>
// mongoexport -d <db_name> -c <col_name> -o <path_to_destination_with_.json extension>
// 

// import (json)
// mongoimport --db <db_name> --collection <col_name> source_path

// csv format
// backup(csv)
// mongoexport --db<db_name> --collection <col_name> --type=csv --query='key-value' --fields 'coma seperated header tag' --out 'path_to_destination_with.csv extension'

// restore(csv)
// mongoimport --db <db_name> --collection <col_name> --type=csv source_path --headerline

// mongodump mongorestore mongoexport and mongoimport
//############# database BACKUP & RESTORE #############


