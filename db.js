const mongoose = require('mongoose');
const config = require('./configs/db.config');

let local_db_url = config.conxnURL + '/' + config.dbName;
let remote_db_url = 'mongodb://group19db:group19db@ds263368.mlab.com:63368/group19db'
// if (process.env.db == 'remote') {
//     db_URL = removeEventListener
// } else {
//     db_URL = local_db_url

// }

// var finalURL = process.env.MONGOLAB_URI || db_URL;
mongoose.connect(process.env.MONGOLAB_URI || remote_db_url,
    { useUnifiedTopology: true, useNewUrlParser: true });
mongoose.connection.once('open', function () {
    console.log('db connection success');
})
mongoose.connection.on('err', function (err) {
    console.log('db connection failed');
})

    // advantages of using ODM
    // schema based solution(db modelling)
    // methods 
    // middleware
    // indexing is easier (required ,unique)
    // Data types
