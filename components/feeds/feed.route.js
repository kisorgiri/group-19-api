const router = require('express').Router();
const FeedCtrl = require('./feed.controller');
const upload = require("./../../middlewares/uploader");
const authenticate = require('./../../middlewares/authenticate');
router.route('/')
    .get(authenticate, FeedCtrl.find)
    .post(authenticate, upload.single('img'), FeedCtrl.insert);


router.route('/search')
    .get(FeedCtrl.search)
    .post(FeedCtrl.search);


router.route('/:id')
    .get(authenticate, FeedCtrl.finById)
    .put(authenticate, upload.single('img'), FeedCtrl.update)
    .delete(authenticate, FeedCtrl.remove);



module.exports = router;