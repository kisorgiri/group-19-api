const FeedModel = require('./feed.model');

function map_feed(feed, feedDetails) {
    if (feedDetails.title)
        feed.title = feedDetails.title;
    if (feedDetails.description)
        feed.description = feedDetails.description;
    if (feedDetails.user)
        feed.user = feedDetails.user;
    if (feedDetails.image)
        feed.image = feedDetails.image

    return feed;
}

function find(condition, pageSize, pageNumber) {
    console.log('pageSize', pageSize);
    console.log('pageCount >>', pageNumber);
    const pageCount = Number(pageSize) || 100;
    const currentPage = (pageNumber || 1) - 1;
    const perPage = pageNumber * pageCount;
    return FeedModel
        .find(condition)
        .limit(pageCount)
        .skip(perPage)
        .populate('user', {
            username: 1,
            email: 1,
            role: 1
        })


}

function insert(data, cb) {
    var newFeed = new FeedModel({});

    var newMappedFeed = map_feed(newFeed, data);
    newMappedFeed.save(function(err, done) {
        if (err) {
            cb(err);
        } else {
            cb(null, done);
        }
    })
}


function update(id, data) {
    return new Promise(function(resolve, reject) {
        FeedModel.findById(id)
            .exec(function(err, feed) {
                if (err) {
                    reject(err);
                }
                if (feed) {
                    const oldImage = feed.image;
                    var updatedFeed = map_feed(feed, data);
                    updatedFeed.save(function(err, updated) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve({
                                oldImage,
                                updated
                            });
                        }
                    })
                } else {
                    reject({
                        msg: 'Feed not found'
                    })
                }
            });
    });
}

function remove(id) {
    return FeedModel.findByIdAndRemove(id);
}

module.exports = {
    find,
    insert,
    update,
    remove
}