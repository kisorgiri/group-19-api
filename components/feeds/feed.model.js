const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const feedSchema = new Schema({
    title: {
        type: String,
        required: true,
        trim: true
    },
    description: String,
    image: String,
    user: {
        type: Schema.Types.ObjectId,
        ref: 'user'
    }
}, {
        timestamps: true
    });
var FeedModel = mongoose.model('feed', feedSchema);
module.exports = FeedModel;