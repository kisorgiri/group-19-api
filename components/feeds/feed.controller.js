const FeedQuery = require('./feed.query');
var fs = require('fs');
var path = require('path');

function find(req, res, next) {
    var condition = {};
    condition.user = req.loggedInUser.id;
    //   TODO
    FeedQuery.find(condition)
        .then(function(data) {
            res.status(200).json(data);
        })
        .catch(function(err) {
            next(err);
        })
}

function finById(req, res, next) {
    var condition = {
        _id: req.params.id
    }
    FeedQuery.find(condition)
        .then(function(data) {
            res.status(200).json(data[0]);
        })
        .catch(function(err) {
            next(err);
        })
}

function search(req, res, next) {
    var searchCondition = {};
    if (req.body.title) {
        searchCondition.title = req.body.title;
    }
    if (req.body.fromDate && req.body.toDate) {
        var fromDate = new Date(req.body.fromDate).setHours(0, 0, 0, 0);
        var toDate = new Date(req.body.toDate).setHours(23, 59, 59, 999);
        searchCondition.createdAt = {
            $gte: new Date(fromDate),
            $lte: new Date(toDate)
        }
    }
    console.log('searchCondition', searchCondition);
    FeedQuery.find(searchCondition, req.query.pageSize, req.query.pageNumber)
        .then(function(data) {
            res.status(200).json(data);
        })
        .catch(function(err) {
            next(err);
        })
}

function insert(req, res, next) {
    var data = req.body;
    if (req.fileError) {
        return next({
            msg: 'invalid file format'
        })
    }
    if (req.file) {
        // optional when filefilter is used
        // var mimetype = req.file.mimetype.split('/')[0];
        // if (mimetype !== 'image') {
        //     fs.unlink(path.join(process.cwd(), 'uploads/images/' + req.file.filename), function (err, done) {
        //         if (err) {
        //             console.log('err');
        //         } else {
        //             console.log('removed');
        //         }
        //     })
        //     return next({
        //         msg: 'Invalid file format'
        //     })
        // }

        data.image = req.file.filename;
    }

    // TODO add user information form here 
    data.user = req.loggedInUser._id;
    // TODO file upload >> add image name
    FeedQuery.insert(data, function(err, saved) {
        if (err) {
            return next(err);
        }
        res.status(200).json(saved);
    })
}

function update(req, res, next) {

    // business logic
    var data = req.body;
    if (req.fileError) {
        return next({
            msg: 'invalid file format'
        })
    }
    if (req.file) {
        data.image = req.file.filename;
    }
    FeedQuery.update(req.params.id, data)
        .then(function(data) {
            if (req.file) {
                fs.unlink(path.join(process.cwd(), 'uploads/images/' + data.oldImage), function(err, done) {
                    if (err) {
                        console.log('err');
                    } else {
                        console.log('removed');
                    }
                })
            }
            res.status(200).json(data);
        })
        .catch(function(err) {
            next(err);
        })
}

function remove(req, res, next) {
    FeedQuery.remove(req.params.id)
        .then(function(data) {
            res.status(200).json(data);
        })
        .catch(function(err) {
            next(err);
        })
}

module.exports = {
    find,
    finById,
    search,
    insert,
    update,
    remove
}